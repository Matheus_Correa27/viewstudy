package br.com.inchurch.view_study

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class MainActivityViewModel : ViewModel() {

    private val wordsList = arrayListOf(
        "public", "private", "fun", "inline",
        "var", "val", "operator", "suspend"
    )

    private var count = MutableLiveData<Int>()
    private var result = MutableLiveData<ArrayList<String>>()

    init {
        result.value = arrayListOf()
    }

    private fun checkWord(word: String): Boolean {
        return wordsList.contains(word) && !result.value!!.contains(word)
    }

    fun getResult() = result

    fun getCount() = count

    fun updateCount() {
        count.value = result.value!!.size
    }

    fun getCountValue() = count.value!!


    fun leftWords() = wordsList.size - getCountValue()

    fun addWord(word: String) {
        if (checkWord(word)) {
            result.value?.add(word)
            result.apply {
                postValue(value)
            }
        }
    }

    fun saveWord(): String {
        return if (result.value.isNullOrEmpty())
            " "
        else
            result.value!!.reduce { acc, s -> "$acc $s" }
    }

    fun clearList() {
        result.value?.clear()
        result.apply {
            postValue(value)
        }
    }

    fun printResult(): String {
        return if (result.value.isNullOrEmpty())
            " "
        else
            result.value!!.reduce { acc, s -> "$acc $s" }
    }

}