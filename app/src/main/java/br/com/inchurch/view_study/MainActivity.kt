package br.com.inchurch.view_study

import android.os.Bundle
import androidx.activity.viewModels
import androidx.appcompat.app.AppCompatActivity
import br.com.inchurch.view_study.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding
    private val viewModel: MainActivityViewModel by viewModels()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        binding = ActivityMainBinding.inflate(layoutInflater)
        val view = binding.root
        setContentView(view)

        viewModel.getResult().observe(this, { showList() })
        viewModel.getCount().observe(this, { printCount() })

        addWord()
        resetList()
        saveWord()
    }

    private fun printCount() {
        binding.wordsCount.text = getString(R.string.words_in_list, viewModel.getCountValue())
        binding.leftWords.text = getString(R.string.words_left, viewModel.leftWords())
    }

    private fun showList() {
        binding.textViewWord.text = viewModel.printResult()
        viewModel.updateCount()
    }

    private fun addWord() {
        binding.button.setOnClickListener {
            val word = binding.editText.text.toString()
            viewModel.addWord(word)
        }
    }

    private fun saveWord() {
        binding.buttonSave.setOnClickListener {
            binding.textViewSaveWord.text = viewModel.saveWord()
        }
    }

    private fun resetList() {
        binding.buttonReset.setOnClickListener {
            viewModel.clearList()
        }
    }

}